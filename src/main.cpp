#include <GLFW/glfw3.h>
#include <iostream>
#include <unistd.h>
#include <cmath>

int
main ()
{
  const float steps = 200;
  const float angle = M_PI * 2.f / steps;
  
  GLFWwindow *window;
  if (!glfwInit ())
    {
      std::cerr << "GLFW failed to initalise!" << std::endl;
      exit (EXIT_FAILURE);
    }

  GLFWmonitor* monitor = glfwGetPrimaryMonitor ();
  const GLFWvidmode* mode = glfwGetVideoMode (monitor);
  glfwWindowHint (GLFW_RED_BITS, mode->redBits);
  glfwWindowHint (GLFW_GREEN_BITS, mode->greenBits);
  glfwWindowHint (GLFW_BLUE_BITS, mode->blueBits);
  glfwWindowHint (GLFW_REFRESH_RATE, mode->refreshRate);

  window = glfwCreateWindow (mode->width, mode->height, PACKAGE, monitor, NULL);
  if (!window)
    {
      glfwTerminate ();
      std::cerr << "Failed to create window!" << std::endl;
      exit (EXIT_FAILURE);
    }
  
  glfwMakeContextCurrent(window);

  glfwSetInputMode (window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  float
    x_pos = 0,
    y_pos = 0,
    radius = 1.0f;
  
  /* Render loop. */
  while (!glfwWindowShouldClose (window))
    {      
      glClearColor (1.0, 1.0, 1.0, 0);
      glClear (GL_COLOR_BUFFER_BIT);

      float
	prev_x = x_pos,
	prev_y = y_pos - radius; 

      for (int i = 0; i <= steps; i++)
	{
	  float
	    new_x = radius * sin (angle * i),
	    new_y = -radius * cos (angle * i);
	  
	  glBegin (GL_TRIANGLES);
	  glColor3f (0, 0.5f, 0);
	  glVertex3f (0.0f, 0.0f, 0.0f);
	  glVertex3f (prev_x, prev_y, 0.0f);
	  glVertex3f (new_x, new_y, 0.0f);
	  glEnd ();

	  prev_x = new_x;
	  prev_y = new_y;
	}
      
      glfwSwapBuffers (window);
      glfwPollEvents ();
    }
}
